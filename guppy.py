#!/usr/bin/env python3
#
# Copyright (C) 2010-2016 Svetlana A. Tkachenko <svetlana@members.fsf.org>
# Copyright (C) 2010-2016 David Vo <david.vo2@gmail.com>
# Copyright (C) 2010-2016 aLasterShark <a.laser.sharky@gmail.com>
# Copyright (C) 2016 Nicolas Aragone <niquito.larigone@gmail.com>
# Copyright (C) 2016 Nathaniel Olsen <nolsen@openmailbox.org>
# Copyright (C) 2017 Jason Harrison <me@jasonharrison.us>
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.


import sys

if sys.version_info[0] < 3:
    print("Error: guppy only works with Python 3+. Please install python 3 and try again.")
    exit()


import os
import configparser
import asyncore
import irc
import time


class main(object):
    def __init__(self):
        # Constant.
        self.version = "0.4.4.4"
        self.homedir = os.path.abspath(os.path.dirname(__file__))
        print("  __ _ _   _ _ __  _ __  _   _ ")
        print(" / _` | | | | '_ \| '_ \| | | | http://puszcza.gnu.org.ua/projects/guppy/")
        print("| (_| | |_| | |_) | |_) | |_| | irc://irc.freenode.net/guppy")
        print(" \__, |\__,_| .__/| .__/ \__, | http://guppy.uk.to")
        print(" |___/      |_|   |_|    |___/  version " + self.version)
        print("PID: " + str(os.getpid()))
        self.directory = self.homedir + '/conf/'
        print("Settings directory: " + self.directory)

        if not os.path.exists(self.directory):
            os.makedirs(self.directory)
        self.configpath = self.directory + 'main.cfg'
        self.logpath = self.directory + 'main.log'

        helptext = """
usage: guppy.py [-h] [-c] [-v]

Start guppy - a modular Python IRC bot.

optional arguments:
  -h, --help      show this help message and exit
  -c, --makeconf  generate a new configuration file
  -v, --version   display version information and exit.
"""
        if "-h" in sys.argv[1:] or "--help" in sys.argv[1:]:
            print(helptext)
            exit()

        if "-v" in sys.argv[1:] or "--version" in sys.argv[1:]:
            exit()

        if "-c" in sys.argv[1:] or "--makeconf" in sys.argv[1:]:
            self.makeconf()

        if not os.path.isfile(self.configpath):
            print("No configuration file found, running makeconf")
            self.makeconf()

        self.config = configparser.RawConfigParser()
        self.config.read(self.configpath)

    def start(self):
        self.clients = []
        for section in self.config.sections():
            server_config = {"network": section, "version": self.version, "confdir": self.directory}
            for item, value in self.config.items(section):
                server_config[item] = value

            print("%s [%s] Connecting..." % (time.strftime("%Y-%m-%d %H:%M:%S"), server_config["network"]))
            self.clients.append(irc.IRC(server_config))
        asyncore.loop()
        
    def stop(self):
        for client in self.clients:
            client.doQuit("Keyboard interrupt.")
            client.pluginManager.unloadAllPlugins()

    def my_raw(self, prompt="", default=""):
#        ret = eval("'" + input("%s [%s]: " % (prompt, default)) + "'")
        ret = input("%s [%s]: " % (prompt, default))
        return default if ret == "" else ret

    def set(self, section, option, text, default=""):
        try:
            if default == "":
                default = self.config.get(section, option)
        except configparser.NoOptionError:
            pass

        value = self.my_raw(text, default)
        self.config.set(section, option, value)

    def makeconf(self):
        self.config = configparser.RawConfigParser()
        getting_servers = True
        while getting_servers:
            currentNetwork = self.my_raw("What IRC network do I connect to (just the name)?")
            try:
                self.config.add_section(currentNetwork)
            except configparser.DuplicateSectionError:
                overwrite = self.my_raw("Server already exists! Overwrite? (yes/no)", "no").lower()
                while overwrite != "yes" and overwrite != "y" and overwrite != "no" and overwrite != "n":
                    overwrite = self.my_raw("Invalid option. Overwrite configuration for " + currentNetwork + "? (yes/no)", "no").lower()

                if overwrite.lower() == "no" or overwrite.lower() == "n":
                    continue  # go back to top of "while getting_servers:"
                #else continue out of try/except

            self.set(currentNetwork, 'host', "What is the IRC network address (eg. irc.example.org)?")
            self.set(currentNetwork, 'channels', "What channels to automatically join (comma-separated, no spaces)?")
            self.set(currentNetwork, 'nickname', "What nickname should I use?")
            self.set(currentNetwork, 'ident', "What username (ident) should I use?")
            self.set(currentNetwork, 'gecos', "What realname (gecos) should I use?")
            self.set(currentNetwork, 'umodes', "What user modes should I enable upon connect?")
            self.set(currentNetwork, 'owner_nick', "What is YOUR nick (used to auth, you can set password later)?")
            self.set(currentNetwork, 'ns_name', "What is my NickServ username (if there is none, press ENTER)?")
            self.set(currentNetwork, 'ns_pwd', "What is my NickServ password (if there is none, press ENTER)?")
            self.set(currentNetwork, 'srpass', "What server password should I use (used on private servers or for Services auth)?")
            self.set(currentNetwork, 'port', "What port should I use? (+ for SSL)", "6667")
            self.set(currentNetwork, 'ipv6', 'Should I use IPv6 to connect (yes/no)?', 'no')
            self.set(currentNetwork, "comchar", "What command char (other than my nick) should I respond to?", "-")
            self.set(currentNetwork, "infochar", "What command char should I use for the info plugin?", "~")
            list1 = os.listdir(self.homedir + "/plugins")
            list2 = [item.replace(".py", "") for item in list1 if not '__' in item]
            print("Available plugins are: " + " ".join(list2))
            self.set(currentNetwork, "plugins", "What plugins will I load on startup (comma-separated, no spaces)?", "auth,printer,pluginloader,ping_server")

            another_server = self.my_raw("All done with " + currentNetwork + ". Add another server? (yes/no)", "no").lower()
            while another_server != "yes" and another_server != "y" and another_server != "no" and another_server != "n":
                another_server = self.my_raw("Invalid option. Do you want to add another server? (yes/no)", "no").lower()

            if another_server == "no" or another_server == "n":
                break
            #else no action needed

        self.writeconfig()
        print('Configuration file has been created.')
        exit()

    def writeconfig(self):
        configfile = open(self.configpath, 'w')
        self.config.write(configfile)
        configfile.close()

if __name__ == "__main__":
    c = None
    c=main()
    try:
        c.start()
    except KeyboardInterrupt:
        print("Shutting down all connections...")
        c.stop()
        print("Quitting!")
