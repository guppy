# Copyright (C) 2010-2016 Svetlana A. Tkachenko <svetlana@members.fsf.org>
# Copyright (C) 2010-2016 David Vo <david.vo2@gmail.com>
# Copyright (C) 2011 FurryHead <furryhead14@yahoo.com>
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.

# This file is a derivative of printer.py plugin in this folder. Credit to original authors is given above.


@plugin
class raw(object):
    def __init__(self, server):
        self.server = server
        self.commands = []
        self.log = server.log

        self.server.handle("data", self.handle_data)
        self.server.handle("send", self.handle_send)

    def handle_data(self, server, data):
        self.log("[%s] < %s" % (server, data), 'irc raw in')
    def handle_send(self, server, data):
        self.log("[%s] > %s" % (server, data), 'irc raw out')
                

