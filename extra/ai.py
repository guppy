# Copyright (C) 2010-2016 Svetlana A. Tkachenko <svetlana@members.fsf.org>
# Copyright (C) 2011 David Vo <david.vo2@gmail.com>
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.

import aiml

# We need the decorator, otherwise the
# bot doesn't see it's a plugin
@plugin
class AI(object):
    # The class name is the plugin name, case insensitive
    """Provides AI chat features via the 'ai' command. Requires python3-aiml. Does not learn?"""
    def __init__(self, server):
        self.server = server
        self.prnt = server.prnt
        self.commands = ["ai"]
        # Create the kernel and learn AIML files
        self.kernel = aiml.Kernel()
        self.kernel.learn("./conf/ai/std-startup.xml")
        self.kernel.respond("load aiml b")
        for h in self.server.handlers:
            func = getattr(self, "handle_" + h, None)
            if func is not None:
                if h == "command":
                    self.server.handle(h, func, self.commands)
                else:
                    self.server.handle(h, func)
    def handle_command(self, channel, user, cmd, args):
        """
        Called when we get a command
        It will be called when we are addressed, as well,
        thus allowing "Guppy: <command>" without the
        comchar prefix.
        """
        #in case we are handling a lot of commands
        if cmd == "ai":
            #send a response
            reply = self.kernel.respond(' '.join(args))
            self.server.doMessage(channel, user + ": " + reply) #Pong. You said '"+" ".join(args)+"'")
